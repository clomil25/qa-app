# DebateBot Django Search


### Setup

1. Launch the aws instance with the correct dbot-django image in Asia Pacific or Central

    a. Password for vncserver is dbot54
    
2. VNCserver automatically starts up with instance so you can connect using the domain name
for the instance.
3. Env is setup with conda environment dbot for aws.
4. Project Directory is located in dbot folder on Desktop. Model files for training can be put in rnet
folder to separate from django project.

```
source activate dbot
```

2. Then from project directory to run server locally
```
python manage.py runserver
```


### Website
1. You will see two fields for the question and article.
2. Input these and submit, then it will load and return the results on the same page.

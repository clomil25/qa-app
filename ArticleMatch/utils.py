from django.core.cache import cache


def article_search(query):
    # token_docs, docs = documents_load()
    # dictionary = Dictionary(token_docs)
    # corpus = [dictionary.doc2bow(doc) for doc in token_docs]
    # tf_idf = TfidfModel(corpus)
    # sim = Similarity('ArticleMatch/Data', tf_idf[corpus], num_features=len(dictionary))
    # sim.num_best = 5

    tf_idf = cache.get('tf_idf')
    dictionary = cache.get('dictionary')
    sim = cache.get('matrix_sim')
    documents = cache.get('documents')

    stoplist = set('for a of the and to in'.split())
    query_doc = [word for word in query.lower().split() if word not in stoplist]
    query_doc_bow = dictionary.doc2bow(query_doc)
    query_doc_tf_idf = tf_idf[query_doc_bow]

    best_doc_indexes = sim[query_doc_tf_idf]
    best_docs = [documents[i] for i, j in best_doc_indexes]
    return best_docs



from django.apps import AppConfig
from django.core.cache import cache

import pandas as pd

from gensim.corpora import Dictionary
from gensim.models import TfidfModel
from gensim.similarities.docsim import MatrixSimilarity


local_dir = '/Users/anicolas/Downloads/articles1.csv'
dev_dir = '/home/ubuntu/Downloads/articles1.csv'


class ArticlematchConfig(AppConfig):
    name = 'ArticleMatch'

    ''' Loads corpus into memory, processed and in original form
    '''

    def documents_load(self):
        data = pd.read_csv(dev_dir)
        documents = list(data['content'])
        token_docs = self.clean(documents)

        # token_docs = cache.get('token_docs')
        dictionary = Dictionary(token_docs)
        corpus = [dictionary.doc2bow(doc) for doc in token_docs]
        tf_idf = TfidfModel(corpus)
        matrix_sim = MatrixSimilarity(tf_idf[corpus], num_features=len(dictionary), num_best=5)

        cache.set('matrix_sim', matrix_sim)
        cache.set('tf_idf', tf_idf)
        cache.set('corpus', corpus)
        # cache.get_or_set('token_docs', token_docs)
        cache.set('documents', documents)
        cache.get('dictionary', Dictionary(token_docs))
        # return token_docs, documents

    ''' Cleans each document - removing stop words, commas, and lowercases each word
    '''
    def clean(self, documents):
        stoplist = set('for a of the and to in'.split())
        texts = [[word for word in document.lower().split() if word not in stoplist]
                 for document in documents]

        # remove words that appear only once
        from collections import defaultdict
        frequency = defaultdict(int)
        for text in texts:
            for token in text:
                frequency[token] += 1

        texts = [[token for token in text if frequency[token] > 1]
                 for text in texts]

        from pprint import pprint  # pretty-printer
        # pprint(texts[5])
        return texts

    def ready(self):
        print("#### Loading Docs ####")
        self.documents_load()



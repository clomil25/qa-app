from rest_framework.decorators import api_view, detail_route, list_route
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.renderers import TemplateHTMLRenderer

from QA.utils import fact_search
from ArticleMatch.utils import article_search


@api_view(["POST"])
def search(request):  # TODO add user model, save user search to db
    # TODO How can I update the search results dynamically, quickly, and user friendly
    question = request.POST.get('search_query')
    articles = article_search(question)
    # article = request.POST.get('article')
    answers = []
    for article in articles:
        answers.append(fact_search(question, article))
    # answers = fact_search(question, article)
    return Response({'Ans': str(answers)}, template_name='home/forty.html')


class SearchViewSet(APIView):

    renderer_classes = (TemplateHTMLRenderer,)
    def get(self, request: object) -> object:
        # TODO check for user id, return special home page based on user
        return Response({'blah': "blah"}, template_name='forty.html')



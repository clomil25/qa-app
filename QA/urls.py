from django.conf.urls import url, include
from rest_framework import routers

from QA.viewsets import SearchViewSet, search


router = routers.SimpleRouter()
router.register('^$', SearchViewSet, base_name="search")

urlpatterns = [
    url(r'^$', SearchViewSet.as_view(), name="home"),
    # url(r'search', SearchViewSet.search)
    url(r'search', search, name='search'),

]
# -*- coding: utf-8 -*-
"""
Created on Sat Jan  6 15:40:39 2018

@author: fanat
"""

import numpy as np
import tensorflow as tf
from nltk.tokenize import word_tokenize

from pprint import pprint
import json

from QA import preprocess
from QA.Models import model_rnet
from QA.preprocess import get_word2vec
from ArticleMatch.utils import article_search

from django.core.cache import cache


def fact_search(question, article):

    model = 'rnet'
    debug = False
    dataset = 'dev'
    model_path = 'QA/Models/Save/rnet_model27.ckpt'
    modOpts = json.load(open('QA/Models/config.json', 'r'))['rnet']['dev']
    pprint(modOpts)

    model = model_rnet.R_NET(modOpts)
    input_tensors, loss, acc, pred_si, pred_ei = model.build_model()
    saved_model = model_path

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    new_saver = tf.train.Saver()
    sess = tf.InteractiveSession(config=config)
    new_saver.restore(sess, saved_model)

    pred_data = {}

    EM = 0.0
    F1 = 0.0
    # empty_answer_idx = np.ndarray((modOpts['batch_size'], modOpts['p_length']))
    empty_answer_idx = cache.get('empty_answer_idx')
    w2v_300 = cache.get('w2v_300')
    if w2v_300 is None:
        raise Exception('error')

    '''Code for submitting question
    '''
    a_token = word_tokenize(article)
    q_token = word_tokenize(question)
    paragraph = np.zeros((60, 300, 300))
    question = np.zeros((60, 30, 300))
    empty_answer_idx = np.ndarray((60, 300))

    # w2v_300 = cache.get('w2v_300')
    # sess = cache.get('sess')
    # input_tensors = cache.get('input_tensors')
    # loss = cache.get('loss')
    # acc = cache.get('acc')
    # pred_si = cache.get('pred_si')
    # pred_ei = cache.get('pred_ei')

    for j in range(len(a_token)):
        if j >= 300:
            break
        try:
            paragraph[0][j][:300] = w2v_300[a_token[j]]
        except KeyError:
            pass
        try:
            paragraph[0][j][:300] = w2v_300[a_token[j].capitalize()]
        except KeyError:
            pass
        try:
            paragraph[0][j][:300] = w2v_300[a_token[j].lower()]
        except KeyError:
            pass
        try:
            paragraph[0][j][:300] = w2v_300[a_token[j].upper()]
        except KeyError:
            pass

    for j in range(len(q_token)):
        if j >= 30:
            break
        try:
            question[0][j][:300] = w2v_300[q_token[j]]
        except KeyError:
            pass
        try:
            question[0][j][:300] = w2v_300[q_token[j].capitalize()]
        except KeyError:
            pass
        try:
            question[0][j][:300] = w2v_300[q_token[j].lower()]
        except KeyError:
            pass
        try:
            question[0][j][:300] = w2v_300[q_token[j].upper()]
        except KeyError:
            pass

    predictions_si, predictions_ei = sess.run([pred_si, pred_ei], feed_dict={
        input_tensors['p']: paragraph,
        input_tensors['q']: question,
        input_tensors['a_si']: empty_answer_idx,
        input_tensors['a_ei']: empty_answer_idx,
    })

    parag = a_token
    f1 = []
    pred_tokens = parag[int(predictions_si[0]):int(predictions_ei[0]) + 1]

    #  print(' '.join(pred_tokens))
    # return ' '.join(pred_tokens)

    # fact_search(question='blah', article="blah")
    f = open('test-output', 'w')
    f.write(' '.join(pred_tokens))
    f.close()
    return ' '.join(pred_tokens)
